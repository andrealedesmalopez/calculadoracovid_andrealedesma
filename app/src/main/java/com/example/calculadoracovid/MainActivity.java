package com.example.calculadoracovid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
RadioButton femenino, masculino, pnormal, speso, obesidad, bpeso;
CheckBox hipertension, diabetes, pulmonar, renal, inmuno;
EditText edad;
Button enviar;
int porcentaje, edadN;
String valorPorcen, sexo, padecimientos="", peso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        femenino=(RadioButton) findViewById(R.id.rbtnFem);
        masculino=(RadioButton) findViewById(R.id.rbtnMas);
        bpeso=(RadioButton) findViewById(R.id.rbtnBpeso);
        pnormal=(RadioButton) findViewById(R.id.rbtnNpeso);
        speso=(RadioButton) findViewById(R.id.rbtnSpeso);
        obesidad=(RadioButton) findViewById(R.id.rbtnObesi);
        edad=(EditText) findViewById(R.id.etxtEdad);
        hipertension=(CheckBox) findViewById(R.id.checkPadecimiento1);
        diabetes=(CheckBox) findViewById(R.id.checkPadecimiento2);
        pulmonar=(CheckBox) findViewById(R.id.checkPadecimiento3);
        renal=(CheckBox) findViewById(R.id.checkPadecimiento4);
        inmuno=(CheckBox) findViewById(R.id.checkPadecimiento5);
        enviar=(Button) findViewById(R.id.btnCalcular);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if(femenino.isChecked()||masculino.isChecked()){

                    if(edad.getText().toString().isEmpty()){
                        Toast.makeText(MainActivity.this, "Ingresa tu edad", Toast.LENGTH_LONG).show();
                    }else{
                        edadN=Integer.parseInt(edad.getText().toString());
                        if( edadN<20||edadN>99){
                            Toast.makeText(MainActivity.this, "Ingresa una edad entre 20 y 99 años", Toast.LENGTH_LONG).show();
                        }else {
                            if (bpeso.isChecked() || pnormal.isChecked() || speso.isChecked() || obesidad.isChecked()) {
                                if (Calcular() >= 50) {
                                    valorPorcen = "Alto";
                                } else {
                                    valorPorcen = "Medio";
                                }
                                Intent i = new Intent(v.getContext(),Resultados.class);
                                i.putExtra("porcentaje", valorPorcen);
                                i.putExtra("sexo", sexo );
                                i.putExtra("padecimientos", padecimientos);
                                i.putExtra("edad", String.valueOf(edadN));
                                i.putExtra("peso", peso);
                                startActivity(i);

                                femenino.clearFocus();
                                masculino.clearFocus();
                                bpeso.clearFocus();
                                pnormal.clearFocus();
                                speso.clearFocus();
                                obesidad.clearFocus();
                                edad.setText("");
                                hipertension.clearFocus();
                                diabetes.clearFocus();
                                pulmonar.clearFocus();
                                renal.clearFocus();
                                inmuno.clearFocus();
                            } else {
                                Toast.makeText(MainActivity.this, "Selecciona tu peso", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }else{
                    Toast.makeText(MainActivity.this, "Selecciona tu sexo", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
    public int Calcular(){
        if(femenino.isChecked()){
            porcentaje=edadN-3+calcularPeso()+calcularPadecimientos();
            sexo="Femenino";
        }else if (masculino.isChecked()){
            porcentaje=edadN+7+calcularPeso()+calcularPadecimientos();
            sexo="Masculino";
        }
        return porcentaje;
    }

    public int calcularPeso(){
        int vPeso=0;
        if (bpeso.isChecked()) {
            peso="Bajo";
        }else if(pnormal.isChecked()){
            peso="Normal";
        }else if (speso.isChecked()){
            vPeso= 3;
            peso="Sobrepeso";
        }else if (obesidad.isChecked()){
            vPeso=3;
            peso="Obesidad";
        }
        return vPeso;
    }

    public int calcularPadecimientos(){
        int vPadeci=0;
        if(hipertension.isChecked()){
            vPadeci=vPadeci+2;
            padecimientos=padecimientos.concat("Hipertensión-");
        }
        if (diabetes.isChecked()){
            vPadeci=vPadeci+5;
            padecimientos=padecimientos.concat("Diabetes-");
        }
        if (pulmonar.isChecked()){
            vPadeci=vPadeci+3;
            padecimientos=padecimientos.concat("Enfermedad Pulmonar Obstructiva Crónica (EPOC)-");
        }
        if (renal.isChecked()){
            vPadeci=vPadeci+20;
            padecimientos=padecimientos.concat("Enfermedad Renal Crónica");
        }
        if (inmuno.isChecked()){
            vPadeci=vPadeci+6;
            padecimientos=padecimientos.concat("Inmunosupresión (por ejemplo cáncer, lupus, etc.)");
        }
        return vPadeci;
    }
}