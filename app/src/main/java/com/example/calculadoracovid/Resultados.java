package com.example.calculadoracovid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class Resultados extends AppCompatActivity {
String resultados, sexo, edad, padecimientos, peso;
TextView txtSexo, txtEdad, txtPeso, txtPadeci, txtRiesgo;
Button regresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados);

        resultados  = getIntent().getExtras().getString("porcentaje");
        sexo  = getIntent().getExtras().getString("sexo");
        peso  = getIntent().getExtras().getString("peso");
        edad = getIntent().getExtras().getString("edad");
        padecimientos = getIntent().getExtras().getString("padecimientos");

        txtSexo=(TextView) findViewById(R.id.txtMSexo);
        txtEdad=(TextView) findViewById(R.id.txtMEdad);
        txtPeso=(TextView) findViewById(R.id.txtMPeso);
        txtPadeci=(TextView) findViewById(R.id.txtMPadeci);
        txtRiesgo=(TextView) findViewById(R.id.txtRiesgo);
        regresar=(Button) findViewById(R.id.btnRegresar);
        txtRiesgo.setText(resultados);
        txtSexo.setText("Sexo: ".concat(sexo));
        txtPeso.setText("Peso: ".concat(peso));
        txtPadeci.setText("Padecimientos: ".concat(padecimientos));
        txtEdad.setText("Edad: ".concat(edad));

        regresar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v){
               Intent i = new Intent(v.getContext(),MainActivity.class);
               startActivity(i);
           }
        });



    }
}